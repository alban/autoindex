package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"strconv"

	"gitlab.com/alban/autoindex/internal/fs"
	"gitlab.com/alban/autoindex/internal/html"
)

func getenv(key string, fallback string) string {
	value := os.Getenv(key)
	if len(value) == 0 {
		return fallback
	}
	return value
}

func parseBool(str string) bool {
	b, err := strconv.ParseBool(str)
	if err != nil {
		return false
	}

	return b
}

var (
	format    = flag.String("format", getenv("AUTOINDEX_FORMAT", "html"), "Sets the format of a directory listing. (html | json)")
	recursive = flag.Bool("recursive", parseBool(getenv("AUTOINDEX_RECURSIVE", "false")), "display directories recursively")
)

func httpHandler(w http.ResponseWriter, r *http.Request) {
	dirname := path.Join(flag.Arg(0), r.URL.Path[1:])
	log.Println(r.Method, r.URL.Path)

	if r.Method != "GET" {
		w.WriteHeader(405)
		fmt.Fprintln(w, "405 Method Not Allowed")
		return
	}

	fileInfo, err := os.Stat(dirname)
	if err != nil {
		w.WriteHeader(404)
		fmt.Fprintln(w, "404 Not Found")
		return
	}

	if !fileInfo.IsDir() {
		http.ServeFile(w, r, dirname)
	} else {
		items, _ := fs.ReadDir(dirname, *recursive)

		if *format == "html" {
			html.Render(r.URL.Path, items, w)
		} else {
			j, _ := json.Marshal(items)
			w.Header().Add("Content-Type", "application/json")
			w.Write(j)
		}
	}
}

func main() {
	flag.Parse()

	directory, _ := filepath.Abs(flag.Arg(0))
	log.Println("autoindex serving " + directory)

	http.HandleFunc("/", httpHandler)
	log.Fatal(http.ListenAndServe(":8080", nil))
}
