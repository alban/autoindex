FROM golang:1.16-alpine as build

WORKDIR /go/src/app
COPY . .

RUN go get -d -v ./...
RUN go install -v ./...

FROM alpine

COPY --from=build /go/bin/autoindex /usr/bin/autoindex

VOLUME ["/public"]
EXPOSE 8080

CMD ["autoindex", "/public"]