package fs

import (
	"io/ioutil"
	"path"
	"time"
)

type FileInfo struct {
	Name     string     `json:"name"`
	Type     string     `json:"type"`
	MTime    string     `json:"mtime,omitempty"`
	Size     int64      `json:"size,omitempty"`
	Children []FileInfo `json:"children,omitempty"`
}

func MapFileInfo(vs []FileInfo, f func(FileInfo) FileInfo) []FileInfo {
	m := make([]FileInfo, len(vs))
	for i, v := range vs {
		m[i] = f(v)
	}
	return m
}

func ReadDir(dirname string, recursive bool) ([]FileInfo, error) {
	items := []FileInfo{}

	files, err := ioutil.ReadDir(dirname)
	if err != nil {
		return nil, err
	}

	for _, file := range files {
		item := FileInfo{
			Name:  file.Name(),
			Type:  "file",
			MTime: file.ModTime().Format(time.RFC1123),
		}

		if file.IsDir() {
			item.Type = "directory"
		} else {
			item.Size = file.Size()
		}

		if file.IsDir() && recursive {
			children, err := ReadDir(path.Join(dirname, item.Name), recursive)

			if err != nil {
				return nil, err
			}

			item.Children = children
		}

		items = append(items, item)
	}

	return items, nil
}
