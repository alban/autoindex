package html

import (
	"fmt"
	"net/http"

	"gitlab.com/alban/autoindex/internal/fs"
)

func Render(dirname string, items []fs.FileInfo, w http.ResponseWriter) {
	fmt.Fprintln(w, "<html>")
	fmt.Fprintln(w, "<head><title>Index of "+dirname+"</title></head>")
	fmt.Fprintln(w, "<body>")
	fmt.Fprintln(w, "<h1>Index of "+dirname+"</h1><hr><pre><a href=\"../\">../</a>")
	renderList(items, w)
	fmt.Fprintln(w, "</pre><hr></body>")
	fmt.Fprintln(w, "</html>")
}

func renderList(items []fs.FileInfo, w http.ResponseWriter) {
	for _, item := range items {
		if item.Type == "file" {
			fmt.Fprintln(w, "<a href=\""+item.Name+"\">"+item.Name+"</a>") // TODO date & size
		} else {
			if item.Children != nil {
				for _, child := range item.Children {
					child.Name = item.Name + "/" + child.Name
				}
				renderList(fs.MapFileInfo(item.Children, func(i fs.FileInfo) fs.FileInfo {
					i.Name = item.Name + "/" + i.Name
					return i
				}), w)
			} else {
				fmt.Fprintln(w, "<a href=\""+item.Name+"/\">"+item.Name+"/</a>")
			}
		}
	}
}
